from django.urls import path

from todos.views import (
    TodoListUpdateView,
    TodoListDeleteView,
    TodoItemCreateView,
    TodoListListView,
    TodoItemDetailView,
    TodoCreateView,
    TodoItemUpdateView,
)

urlpatterns = [
    path("", TodoListListView.as_view(), name="list_todos"),
    path("<int:pk>/", TodoItemDetailView.as_view(), name="detail_todos"),
    path("create/", TodoCreateView.as_view(), name="create_todolist"),
    path("<int:pk>/edit", TodoListUpdateView.as_view(), name="edit_todos"),
    path("<int:pk>/delete", TodoListDeleteView.as_view(), name="delete_todos"),
    path("<int:pk>/new", TodoItemCreateView.as_view(), name="create_todoitem"),
    path("", TodoListListView.as_view(), name="home"),
    path("<int:pk>/updateitem", TodoItemUpdateView.as_view(), name="updateitem_items"),
]
