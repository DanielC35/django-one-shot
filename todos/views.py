from django.shortcuts import render
from django.views.generic import ListView, DetailView, CreateView, UpdateView, DeleteView

from todos.models import TodoItem, TodoList


# Create your views here.
class TodoListListView(ListView):
    model = TodoList
    context_object_name = "todolist"
    template_name = "todos/list.html"
    paginate_by = 10


class TodoItemDetailView(DetailView):
    model = TodoList
    template_name = "todos/detail.html"


class TodoCreateView(CreateView):
    model = TodoList
    template_name = "todos/create.html"
    fields = ["name"]
    success_url = "/todos"

    def form_valid(self, form):
        form.instance.author = self.request.user
        return super().form_valid(form)


class TodoListUpdateView(UpdateView):
    model = TodoList
    template_name = "todos/edit.html"
    fields = ["name"]
    success_url = "/todos"


class TodoListDeleteView(DeleteView):
    model = TodoList
    template_name = "todos/delete.html"
    success_url = "/todos"


class TodoItemCreateView(CreateView):
    model = TodoItem
    template_name = "items/new.html"
    success_url = "/todos"
    fields = ["task", "due_date", "is_completed", "list"]

    def form_valid(self, form):
        form.instance.author = self.request.user
        return super().form_valid(form)


class TodoItemUpdateView(UpdateView):
    model = TodoItem
    template_name = "items/updateitem.html"
    success_url = "/todos"
    fields = ["task", "due_date", "is_completed", "list"]
